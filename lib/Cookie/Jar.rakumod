class Cookie::Jar { ... }

my class Cookie::Jar::Cookie does Associative {
    trusts Cookie::Jar;

    has DateTime $.creation-time;
    has Str      $.domain;
    has DateTime $.expires;
    has Bool     $.hostonly;
    has Bool     $.httponly;
    has DateTime $.last-access-time;
    has Str      $.name;
    has Str      $.path;
    has Bool     $.secure;
    has Str      $.value;

    # Cookie extensions are stored as a hash of pairs, where the top
    # level is the key of the attribute canonicalised to lower-case
    # and the value is a Pair whose key is the name with the original
    # casing (for serialisation) and the value is the attribute value.
    # Attributes with no value (=flags) have the value set to Any
    has %!extensions is built;

    method new ( Str:D $url, Str:D $cookie-string --> ::?CLASS ) {
        my $now = now.DateTime;

        my ( $name, $value, $cookie ) = parse-cookie $cookie-string;

        return ::?CLASS unless $name;

        my ( $, $host, $path ) = split-url $url;

        # check and normalize domain
        normalize-domain $host, $cookie or return ::?CLASS;

        # normalize path
        $cookie<path> = default-path $path
            unless $cookie<path>:exists && $cookie<path>.starts-with: '/';

        if $cookie<max-age>:exists {
            # "If delta-seconds is less than or equal to zero (0), let expiry-time
            # be the earliest representable date and time." RFC 6265 § 5.2.2
            $cookie<expires> = $cookie<max-age> <= 0 ?? DateTime.new(0) !! $now + $cookie<max-age>;
            $cookie<max-age>:delete;
        }

        my %args = $cookie< path expires httponly hostonly secure domain >:delete:kv;

        %args<creation-time> = %args<last-access-time> = $now;
        %args<extensions>    = $cookie;

        self.bless: |%args, :$name, :$value;
    }

    method !new ( Str:D $data, Str:D $meta --> ::?CLASS ) {
        my $now = now.DateTime;

        my ( $name, $value, $cookie ) = parse-cookie $data;

        # Ignore cookies that we cannot parse
        return ::?CLASS unless $name;

        my ( $me, $ta, $other ) = parse-cookie $meta;

        # Check me=ta magic
        return ::?CLASS unless $me eq 'me' && $ta eq 'ta';

        my %args = $cookie< path expires httponly hostonly secure domain >:delete:kv;

        # Fields parsed from the meta section are stored as extensions
        # since they are not well-known cookie attributes
        for < last-access-time creation-time > {
            my $value = .defined ?? .value.&parse-date !! $now given $other{$_};;
            %args{$_} = $value;
        }

        %args<extensions> = $cookie;

        return self.bless: |%args, :$name, :$value;
    }

    method persistent ( --> Bool ) { $!expires.defined }

    method http-only () { $!httponly }
    method host-only () { $!hostonly }

    method expired ( DateTime $now? --> Bool ) {
        $!expires ?? $!expires <= ( $now // now.DateTime ) !! False;
    }

    method get ( Str:D $key is copy ) {
        die 'Cannot retrieve a cookie attribute without a name'
            unless $key;

        $key .= lc;

        return .value === Any ?? True !! .value with %!extensions{$key};

        return .(self) with self.^can($key).first;

        return False;
    }

    method !match (
             Str:D $scheme,
             Str:D $host,
             Str:D $path,
        DateTime:D $now,
             Set   $names?,
        --> Bool
    ) {
        return False if $names     && !$names{ $!name };
        return False if $!hostonly && $!domain ne $host;
        return False if $!secure   && $scheme  ne 'https';
        return False if $.expired: $now;

        return False unless domain-match $host,        $!domain;
        return False unless   path-match $path || '/', $!path;

        True;
    }

    method !touch { $!last-access-time = now.DateTime }

    method Str ( --> Str ) {
        my $line = "$!name=$!value";

        $line ~= "; Expires={ .&format-date }" with $!expires;
        $line ~= "; Domain=$_"                 with $!domain;
        $line ~= "; Path={ .&url-encode }"     with $!path;

        if %!extensions.elems {
            $line ~= '; ' ~ %!extensions
                .pairs.sort( *.key )
                .map( *.value )
                .map({ .value === Any ?? .key !! "{ .key }={ .value }" })
                .join: '; ';
        }

        $line ~= '; Secure'   if $!secure;
        $line ~= '; HttpOnly' if $!httponly;
        $line ~= '; HostOnly' if $!hostonly;

        # Print private meta fields
        $line ~= ';;; me=ta';
        $line ~= "; Creation-Time={    .&format-date }" with $!creation-time;
        $line ~= "; Last-Access-Time={ .&format-date }" with $!last-access-time;

        $line;
    }

    #
    # Helper subroutines
    #

    my sub parse-cookie ( Str:D $cookie ) {
        my ( $kvp, @attrs ) = $cookie.split: ';';
        $kvp //= '';

        my ( $name, $value ) = $kvp.split( '=', 2 )».trim;

        return Empty unless $name && $value.defined;

        my $parse;
        for @attrs -> $attr {
            next unless $attr;

            my ( $key, $value ) = $attr.split( '=', 2 )».trim;

            $value = do given $key.lc {
                when 'httponly' | 'secure' | 'hostonly' { True }

                when 'expires' { parse-date $value }

                when 'domain' { $value.subst: /^\./ }

                when 'path' { url-decode $value }

                when 'max-age' {
                    next unless $value ~~ / ^ <[ 0..9 - ]> /;
                    Duration.new: $value;
                }

                # Unknown cookie attributes are cookie extensions
                # We clone the value to avoid a pair that points to itself
                default { $key => $value.clone }
            }

            $parse{ $key.lc } = $value;
        }

        return ( $name, $value, $parse );
    }

    # Copied from https://gitlab.com/jjatria/http-tiny/-/blob/master/lib/HTTP/Tiny.rakumod#L372
    my sub url-decode ( Str:D $text --> Str ) {
        return $text.subst: / '%' ( <xdigit> ** 2 ) /,
            { $0.Str.parse-base(16).chr }, :g;
    }

    # Based on https://gitlab.com/jjatria/http-tiny/-/blob/master/lib/HTTP/Tiny.rakumod#L378
    my sub url-encode ( Str:D $text --> Str ) {
        return $text.subst:
            /<-[
                ! * ' ( ) ; : @ + $ , / ? # \[ \]
                0..9 A..Z a..z \- . ~ _
            ]> /,
            { .Str.encode».fmt('%%%02X').join }, :g;
    }

    my sub normalize-domain ( $host, $parse is rw --> Bool() ) {
        state &public-suffix = try {
            require ::('PublicSuffix');
            ::('PublicSuffix').WHO<&public-suffix>;
        }

        my $domain = $parse<domain> // '';

        # From https://www.rfc-editor.org/rfc/rfc6265#section-5.3
        # If the user agent is configured to reject "public suffixes" and
        # the domain-attribute is a public suffix
        if &public-suffix && try public-suffix $parse<domain> -> $suffix {
            if $domain eq $suffix {
                # [Ignore the cookie if] the domain-attribute is [not] identical
                # to the canonicalized request-host
                return if $domain ne $host;
                $parse<domain>:delete;
            }
        }
        # Catches domains like 'localhost' or other single-label domains
        elsif !$domain.contains('.') && $domain eq $host {
            return $parse<hostonly> = True;
        }

        # [Ignore the cookie if] the domain-attribute is non-empty [and] the
        # canonicalized request-host does not domain-match the domain-attribute
        return domain-match $host, $parse<domain> if $parse<domain>;

        $parse<domain> = $host;
        return $parse<hostonly> = True;
    }

    my sub default-path ( Str:D $path --> Str ) {
        return '/' unless $path.starts-with: '/';

        $path ~~ / ^ $<default> = [ .* ] '/' /; # greedy to last /

        return ~$<default> || '/';
    }

    my sub domain-match ( $parent, $child --> Bool ) {
        return True  if $parent eq $child;

        return False unless $parent ~~ / :i <[ a .. z ]> /; # non-numeric

        return $parent.subst( /$child$/ ).ends-with('.') if $parent.ends-with($child);

        return False;
    }

    my sub path-match ( Str:D $request, Str:D $cookie --> Bool ) {
        return $request eq $cookie
            || $request.starts-with($cookie)
            && ( $cookie.ends-with('/') || $request.starts-with: "$cookie/" );
    }

    # Methods to implement Associative
    method     AT-KEY (\key) {         $.get(key) }
    method EXISTS-KEY (\key) { defined $.get(key) }

    # Coercions

    method list { $.Hash.list }

    method Hash ( --> Hash ) {
        my %hash = ( :$!name, :$!value );

        %hash.push: ( :$!creation-time    ) if $!creation-time;
        %hash.push: ( :$!domain           ) if $!domain;
        %hash.push: ( :$!expires          ) if $!expires;
        %hash.push: ( :$!hostonly         ) if $!hostonly.defined;
        %hash.push: ( :$!httponly         ) if $!httponly.defined;
        %hash.push: ( :$!last-access-time ) if $!last-access-time;
        %hash.push: ( :$!path             ) if $!path;
        %hash.push: ( :$!secure           ) if $!secure;

        if %!extensions {
            %hash<extensions> = %!extensions
                .values.map({ .value === Any ?? ( .key => True ) !! $_  })
                .Hash;
        }

        %hash;
    }
}

my constant DoW = [< Mon Tue Wed Thu Fri Sat Sun >];
my constant MoY = [< Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec >];

# Copied from https://gitlab.com/jjatria/http-tiny/-/blob/master/lib/HTTP/Tiny.rakumod#L88
my sub format-date ( DateTime:D $date = now.DateTime --> Str ) {
    sprintf '%s, %02d %s %04d %02d:%02d:%02d GMT',
        DoW[ .day-of-week - 1 ],
        .day-of-month,
        MoY[ .month - 1 ],
        .year,
        .hour,
        .minute,
        .whole-second given $date;
}

# Copied from https://gitlab.com/jjatria/http-tiny/-/blob/master/lib/HTTP/Tiny.rakumod#L101
my sub parse-date ( Str $date --> DateTime ) {
    my %args;
    given $date {
        # Sun, 06 Nov 1994 08:49:37 GMT  ; RFC 822, updated by RFC 1123
        when /
            ^
            <{ DoW }>                     ',' ' '+
            $<day>   =   \d ** 1..2           ' '+
            $<month> = <{ MoY }>              ' '+
            $<year>  =   \d ** 4              ' '+
            $<hms>   = [ \d ** 2 ] ** 3 % ':' ' '+
            GMT
            $
        / {
            %args = $/.hash».Str;
        }

        # Sunday, 06-Nov-94 08:49:37 GMT ; RFC 850, obsoleted by RFC 1036
        when /
            ^
            < Mon | Tues | Wednes | Thurs | Fri | Satur | Sun > 'day,' ' '+
            $<day>   =   \d ** 2                                       '-'
            $<month> = <{ MoY }>                                       '-'
            $<year>  =   \d ** 2                                       ' '+
            $<hms>   = [ \d ** 2 ] ** 3 % ':'                          ' '+
            GMT
            $
        / {
            %args = $/.hash».St;
            %args<year> = $<year> + 1900;
        }

        # Sun Nov  6 08:49:37 1994       ; ANSI C's asctime() format
        when /
            ^
            <{ DoW }>                         ' '+
            $<month> = <{ MoY }>              ' '+
            $<day>   =   \d ** 1..2           ' '+
            $<hms>   = [ \d ** 2 ] ** 3 % ':' ' '+
            $<year>  =   \d ** 4
            $
        / {
            %args = $/.hash».Str;
        }

        default { return DateTime }
    }

    my $hms = %args<hms>:delete;
    my ( $hour, $minute, $second ) = $hms.split(':').map: *.Int;

    my $month = MoY.antipairs.Map.{ %args<month>:delete } + 1;

    return DateTime.new: |%args, :$month, :$hour, :$minute, :$second;
}

# Based on https://gitlab.com/jjatria/http-tiny/-/blob/master/lib/HTTP/Tiny.rakumod#L334
my sub split-url ( Str:D $url ) {
    use IDNA::Punycode;

    $url ~~ /
        ^
        $<scheme>     = <-[ : / ? # ]>+
        '://'
        $<authority>  = <-[   / ? # ]>+
        $<path>       = <-[       # ]>*
    / or die "Cannot parse URL: $url";

    my $scheme = lc $<scheme>;
    die "Unsupported URL scheme: $scheme" unless $scheme eq 'http' | 'https';

    my $path = ~$<path>;

    my ( $host ) = $<authority>.flip.split( '@', 2 )».flip;

    s/ ':' \d* $ // given $host; # Remove port

    return $scheme, $host.lc.split('.')».&encode_punycode.join('.'), $path;
}

=begin pod

=head2 NAME

Cookie::Jar - A minimalist HTTP cookie jar

=head2 SYNOPSIS

=begin code :lang<raku>
use Cookie::Jar;

my $cookies = Cookie::Jar.new;

$cookies.add: 'https://example.com', 'foo=123; Domain=example.com';
$cookies.add: 'https://example.com', 'bar=234';

say "{ .name } -> { .value }" for $cookies.get: 'GET', 'https://example.com';
# OUTPUT:
# foo -> 123
# bar -> 234

# The 'bar' cookie does not apply to this subdomain
say $cookies.header: 'GET', 'https://www.example.com';
# OUTPUT:
# foo=123

# Cookies can persist on filesystem
my $other = $jar.save('cookie.jar').load('cookie.jar');
=end code

=head2 DESCRIPTION

This is a minimal class to manage HTTP cookies. It can be used together
with a HTTP client class like
L<HTTP::Tiny|https://raku.land/zef:jjatria/HTTP::Tiny> to manage cookies
received in HTTP responses and determine the cookies that apply on new
requests.

Unlike other alternatives available at the time of writing, user agents
desiring to integrate with this class do not need to use a specific class
to represent the request, the response, or the cookie: all that is required
is the request URL and the headers received from the server.

The operations on this library should be thread-safe.

=head2 METHODS

Some of the methods below take a request URL parameter. In compliance with
L<RFC 6265|https://datatracker.ietf.org/doc/html/rfc6265>, these are all
canonicalised to their ASCII "punycode" representation before processing.

Likewise, the methods in this class that return cookies (like L<get|#get>
and L<dump|#dump> described below) return a read-only opaque cookie object
with the following available methods:

=head3 The Cookie Object

=defn creation-time
A DateTime marking the moment the cookie was created. This could be before
the jar was created if the cookie was loaded from external storage with
L<load|#load>. This attribute is internal, and does not conflict with an
extension of the same name.

=defn domain
Returns a Str with the cookie's domain. If the cookie did not have a domain
initially, this will be the domain the cookie was received from.

=defn expired
Returns a Bool that will be true if the cookie is persistent and its
expiration date is in the past. It can optionally take a DateTime object
to be used instead of the current timestamp.

=defn expires
Returns a DateTime with the cookie's expiration time. This might be a type
object if the cookie is not persistent.

=defn host-only
Returns a Bool specifying whether the cookie should apply to domains other
than the one in its "domain" attribute. This attribute can also be called as
C<hostonly>.

=defn http-only
Returns a Bool specifying whether this cookie should be included in non-HTTP
requests (this is of little use for Raku user-agents). This attribute can
also be called as C<httponly>.

=defn last-access-time
Returns a DateTime specifying when the cookie was last matched for a given
URL (using the L<get|#get> or L<header|#header> methods). This attribute is
internal, and does not conflict with an extension of the same name.

=defn name
Returns a Str with the cookie name. This attribute is internal, and does not
conflict with an extension of the same name.

=defn path
Returns a Str with the path the cookie applies to.

=defn persistent
Returns a Bool which will be true if the cookie has a defined expiration
date.

=defn secure
Returns a Bool specifying whether this cookie should be sent over non-secure
channels. Cookies with this attribute set to a true value will only be
matched for HTTPS requests.

=defn value
Returns a Str with the value of the cookie. No effort is made by Cookie::Jar
to parse or otherwise decode this value. This attribute is internal, and does
not conflict with an extension of the same name.

=defn get
Takes a key that will be matched case-insensitively to a cookie attribute.
The key can be any of the ones mentioned above, or the name of any of the
cookie's extensions. If the cookie does not have an attribute with that
name this method will return False. Otherwise, the value of the attribute
will be returned, or True if the attribute has no value. If the cookie was
set with an attribute with the same name as one of the fields described as
"internal" above (eg. 'creation-time' and 'last-access-time', etc), using
that key with this method will return the extension.

=head3 new

=begin code :lang<raku>
method new () returns Cookie::Jar
=end code

Creates a new Cookie::Jar object. The constructor takes no parameters.

To construct a Cookie::Jar with cookies that have been saved on a previous
session (with the L<save|#save> method), see the L<load|#load> method below.

=head3 add

=begin code :lang<raku>
method add (
    Str:D $url,
    Str:D $cookie-string,
) returns Bool
=end code

Add a new cookie to the internal storage.

This method takes a URL and a C<Set-Cookie> header string (received from
making a request to that URL) and adds the cookie to the jar. If the cookie
is expired, any matching cookie is deleted as described in
L<RFC 6265 § 4.1.2|https://datatracker.ietf.org/doc/html/rfc6265#section-4.1.2>.

If the L<PublicSuffix|https://raku.land/zef:jjatria/PublicSuffix> module is
available, it will be used to validate cookie domains and reject
L<supercookies|https://en.wikipedia.org/wiki/HTTP_cookie#Supercookie>.

Returns True if the cookie was successfully added, or False otherwise.

=end pod

class Cookie::Jar:ver<0.1.4>:auth<zef:jjatria> {

    has $!lock  = Lock.new;
    has $!store = {};

    method add ( Str:D $url, Str:D $cookie-string --> Bool ) {
        my $cookie = Cookie::Jar::Cookie.new: $url, $cookie-string;
        return False unless $cookie;

        # update creation time from old cookie, if exists
        if $!store{ $cookie.domain }{ $cookie.path }{ $cookie.name } -> $old {
            $cookie .= clone: creation-time => $old.creation-time;
        }

        # if cookie has expired, purge any old matching cookie, too
        if $cookie.expired {
            $!lock.protect: {
                $!store{ $cookie.domain }{ $cookie.path }{ $cookie.name }:delete;
            }
            return True;
        }

        $!lock.protect: {
            $!store{ $cookie.domain }{ $cookie.path }{ $cookie.name } = $cookie;
        }
        return True;
    }

=begin pod

=head3 dump

=begin code :lang<raku>
method dump () returns Hash
=end code

This method offers some introspection by returns a Hash representation of
the internal storage used by the jar.

The data structure store cookies indexed by nested domain, path, and cookie
name keys, as in the following example:

=begin code :lang<raku>
{
    'example.com' => {
        '/path' => {
            'SID' => ..., # see below for details on cookie values
        }
    },
}
=end code

Cookies in this structure (the parts represented by the ellipsis) will be
returned as an internal read-only Cookie object. For details on this Cookie
object, see L<above|#the-cookie-object>.

=end pod

    method dump ( --> Hash ) {
        my %dump;

        $!lock.protect: {
            for $!store.kv -> $domain, $_  {
                for .kv -> $path, $_ {
                    for .kv -> $name, $_ {
                        %dump{ $domain }{ $path }{ $name } = .clone;
                    }
                }
            }
        }

        %dump;
    }

=begin pod

=head3 get

=begin code :lang<raku>
method get (
    Str:D $method,
    Str:D $url,
) returns List
=end code

Takes a request method and URL and returns a (possibly empty) list of cookies
that would apply to a request made to that URL, as described in
L<RFC 6265 § 5.4|https://datatracker.ietf.org/doc/html/rfc6265#section-5.4>.
The order of the cookies in the list follows the same order they would
take in a C<Cookie> header sent with that request, as per the document above.

The method must be one of C<CONNECT>, C<DELETE>, C<GET>, C<HEAD>, C<OPTIONS>,
C<PATCH>, C<POST>, C<PUT>, or C<TRACE>, and will be matched case-sensitively,
as per HTTP/1.1 specification.

Elements in the list will be internal read-only Cookie objects. For
details on this Cookie object, see L<above|#the-cookie-object>.

=end pod

    method get ( Str:D $method, Str:D $url --> List() ) {
        die "Unsupported HTTP method: $method" unless $method eq
            | 'CONNECT'
            | 'DELETE'
            | 'GET'
            | 'HEAD'
            | 'OPTIONS'
            | 'PATCH'
            | 'POST'
            | 'PUT'
            | 'TRACE';

        my ( $scheme, $host, $url-path ) = split-url $url;

        my $now = now.DateTime;

        $!lock.protect: {
            my @cookies;
            for $!store.kv -> $domain, $_  {
                for .kv -> $cookie-path, $_ {
                    for .kv -> $name, $cookie {
                        if $cookie.expired: $now {
                            $!store{ $domain }{ $cookie-path }{ $name }:delete;
                            next;
                        }

                        next unless $cookie!Cookie::Jar::Cookie::match: $scheme, $host, $url-path, $now;

                        $cookie!Cookie::Jar::Cookie::touch;

                        @cookies.push: $cookie.clone
                    }
                }
            }

            @cookies.sort: {
                   $^b.path.chars    <=> $^a.path.chars
                || $^a.creation-time <=> $^b.creation-time
            }
        }
    }

=begin pod

=head3 header

=begin code :lang<raku>
method header (
    Str:D $method,
    Str:D $url,
) returns Str
=end code

Takes a request method and URL and returns a (possibly empty) string suitable
to be used as the C<Cookie> header sent to a request made to that URL. The
order of the cookies in the string follows the order described in
L<RFC 6265 § 5.4|https://datatracker.ietf.org/doc/html/rfc6265#section-5.4>.

The method must be one of C<CONNECT>, C<DELETE>, C<GET>, C<HEAD>, C<OPTIONS>,
C<PATCH>, C<POST>, C<PUT>, or C<TRACE>, and will be matched case-sensitively,
as per HTTP/1.1 specification.

This method calls L<get|#get> internally.

=end pod

    method header ( |c --> Str ) {
        $.get(|c).map({ .name ~ '=' ~ .value }).join: '; '
    }

=begin pod

=head3 clear

=begin code :lang<raku>
multi method clear ( ) returns Cookie::Jar

multi method clear (
    Str $url,
       *@names,
) returns Cookie::Jar
=end code

Clear the internal storage of the jar.

When called with no arguments, all values will be deleted.

This method can alternatively be called with a URL and a possibly empty
list of cookie names. In this case cookies that would match a request
made to that URL (as per the same rules used by the L<get|#get> method),
and possibly one of the names provided, then those matching cookies will
be deleted.

=end pod

    multi method clear ( --> ::?CLASS ) { $!lock.protect: { $!store = {} }; self }

    multi method clear ( Str:D $url, *@names --> ::?CLASS ) {
        my ( $scheme, $host, $path ) = split-url $url;

        my $now = now.DateTime;

        my Set $names;
        $names = set @names if @names;

        $!lock.protect: {
            for $!store{ * ; * } -> $path-store {
                $path-store{
                    $path-store
                        .pairs
                        .grep({
                            .value!Cookie::Jar::Cookie::match:
                                $scheme,
                                $host,
                                $path,
                                $now,
                                $names;
                        })
                        .map: *.key
                }:delete
            }
        }

        self;
    }

=begin pod

=head3 save

=begin code :lang<raku>
method save (
    IO()    $path,
    Bool() :$netscape,
    Bool() :$all,
) returns Cookie::Jar
=end code

Save the internal storage into a cookie file. This file will is suitable
to be read with the L<load|#load> method below. Only persistent cookies
will be saved.

By default, the file will be written using an internal format used by
Cookie::Jar. If the C<:netscape> flag is set to a true value, the file will
instead use the
L<Netscape HTTP Cookie File|https://curl.haxx.se/docs/http-cookies.html>
format, compatible with those created by C<libcurl>.

This method returns the calling object.

=end pod

    multi method save ( IO() $file, :$all, :netscape($) where *.not --> ::?CLASS ) {
        my $fh = $file.open: :w;
        LEAVE try $fh.close;

        $fh.put: q:to/HEADER/;
        # Raku Cookie::Jar HTTP Cookie File
        HEADER

        $fh.put: $_
            for sort { .domain, .path, .name },
                self!all-cookies.grep: { .persistent || $all };

        self;
    }

    multi method save ( IO() $file, :$all, :netscape($) where *.so --> ::?CLASS ) {
        my $fh = $file.open: :w;
        LEAVE try $fh.close;

        $fh.put: q:to/HEADER/;
        # Netscape HTTP Cookie File
        # https://curl.haxx.se/docs/http-cookies.html
        # This file was generated by Raku's Cookie::Jar! Edit at your own risk
        HEADER

        $fh.put: join "\t",
            .domain,
            .host-only  ?? 'FALSE' !! 'TRUE',
            .path,
            .secure     ?? 'TRUE'  !! 'FALSE',
            .persistent ?? .expires.posix !! 0,
            .name,
            .value
            for sort { .domain, .path, .name },
                self!all-cookies.grep: { .persistent || $all };

        self;
    }

=begin pod

=head3 load

=begin code :lang<raku>
method load (
    IO()    $path,
    Bool() :$netscape,
) returns Cookie::Jar
=end code

Load cookies from a cookie file, like that generated with L<save|#save>.

If called on an existing instance, the internal storage will be cleared
before reading the input file. This method can also be called on a
Cookie::Jar type object to be used as a constructor.

In both cases, this method will return a Cookie::Jar object with the
cookies loaded from the read file.

By default, the file is assumed to use the internal Cookie::Jar format.
If the file is found to have a
L<Netscape HTTP Cookie File|https://curl.haxx.se/docs/http-cookies.html>
header instead (like the one generated by C<libcurl>), the file will be
parsed as such. Setting the C<:netscape> flag to a true value enforces this,
such that providing a file missing this header is an error.

=end pod

    multi method load ( ::?CLASS:U: |c --> ::?CLASS ) { self.new.load: |c }

    multi method load ( ::?CLASS:D: IO() $file, :netscape($) where *.not --> ::?CLASS ) {
        $.clear;

        $!lock.protect: {
            for $file.lines {
                FIRST {
                    return $.load: $file, :netscape
                        if /^ '#' ' Netscape'? ' HTTP Cookie File' $/;
                }

                next if .not or .starts-with: '#';

                # Lines like
                # foo=bar; Path=/; Secure; HttpOnly;;; [ PRIVATE COOKIE META DATA ]
                # We strip a possible leading 'Set-Cookie:' so the format is compatible
                # with the files used by curl
                my ( $data, $meta ) = .flip.split( ' ;;;', 2 ).reverse».flip;

                die "Found strange line while reading $file: $_" unless $data && $meta;

                my $cookie = Cookie::Jar::Cookie!Cookie::Jar::Cookie::new: $data, $meta;
                next unless $cookie && $cookie.domain && $cookie.path;

                $!store{ $cookie.domain }{ $cookie.path }{ $cookie.name } = $cookie;
            }
        }

        self;
    }

    multi method load ( ::?CLASS:D: IO() $file, :netscape($) where *.so --> ::?CLASS ) {
        $.clear;

        $!lock.protect: {
            for $file.lines {
                FIRST {
                    die "$file does not appear to be a Netscape HTTP cookie file"
                        unless /^ '#' ' Netscape'? ' HTTP Cookie File' $/;
                }

                next if .not or .starts-with: '#';

                my (
                    $domain,
                    $include-subdomains,
                    $path,
                    $secure,
                    $expires,
                    $name,
                    $value,
                ) = .split: "\t", 7;

                my $data = "$name=$value; Domain=$domain; Path=$path";
                $data ~= '; Secure'   if $secure             eq 'TRUE';
                $data ~= '; HostOnly' if $include-subdomains ne 'TRUE';

                if $expires.Int -> $timestamp {
                    my $dt = DateTime.new: $timestamp, formatter => &format-date;
                    $data ~= "; Expires=$dt";
                }

                my $cookie = Cookie::Jar::Cookie!Cookie::Jar::Cookie::new: $data, 'me=ta';
                next unless $cookie && $cookie.domain && $cookie.path;

                $!store{ $cookie.domain }{ $cookie.path }{ $cookie.name } = $cookie;
            }
        }

        self;
    }

    # Return a list of all cookies
    #                       domain   path    cookie
    #                             \    |    /
    method !all-cookies { $!store{ * ; * ; * }».clone }
}

=begin pod

=head2 LIMITATIONS

Cookie::Jar aims to be I<conditionally compliant> with the
L<HTTP State Management Mechanism [RFC 6265]|https://datatracker.ietf.org/doc/html/rfc6265>.

It aims to meet all "MUST" requirements of the specification, but only some
of the "SHOULD" requirements.

Some particular limitations of note include:

=begin item
Cookie::Jar does no support features that were obsoleted by
L<RFC 6265|https://www.rfc-editor.org/rfc/rfc6265.html>. This includes the
C<Cookie2> and C<Set-Cookie2> headers, C<.local> suffixes, etc.
=end item

=begin item
Since L<RFC 6265|https://www.rfc-editor.org/rfc/rfc6265.html>. is the only
current specification of HTTP cookie behaviour, Cookie::Jar does I<not>
support features that are not described in it. This includes features like
SameSite.
=end item

=head2 SEE ALSO

=head3 Other cookie jars

=head4 Cro::HTTP::Client::CookieJar

Shipped as part of L<Cro::HTTP|https://raku.land/cpan:JNTHN/Cro::HTTP>
and is used by that client. The client cannot be configured to use another
cookie jar.

=head4 HTTP::Cookies

Shipped as part of L<HTTP::UserAgent|https://raku.land/github:sergot/HTTP::UserAgent>
and is used by that client. The client cannot be configured to use another
cookie jar.

=head3 Additional Features

=head4 L<Cookie::Baker|https://raku.land/github:tokuhirom/Cookie::Baker>

Cookie::Baker provides two functions, one of which, C<bake-cookie>,
can be used to convert a Hash into a string that can be used as a C<Cookie>
header. This can be used together with the L<add|#add> method to add a
cookie from a set of key-value pairs.

=head4 L<PublicSuffix|https://raku.land/zef:jjatria/PublicSuffix>

If this distribution is installed, Cookie::Jar will use it to verify cookie
domains against the list of public suffixes, as described in step 5 of
L<RFC 6265 § 5.3|https://www.rfc-editor.org/rfc/rfc6265#section-5.3>. This
should help avoid
L<supercookies|https://en.wikipedia.org/wiki/HTTP_cookie#Supercookie>.

=head2 AUTHOR

José Joaquín Atria <jjatria@cpan.org>

=head2 ACKNOWLEDGEMENTS

Other than the cookie jars mentioned above, the code and API in this
distribution takes inspiration from a number of similar Perl libraries.
In particular:

=item L<HTTP::CookieJar|https://metacpan.org/pod/HTTP::CookieJar>

=item L<HTTP::Cookies|https://metacpan.org/pod/HTTP::Cookies>

=item L<Mojo::UserAgent::CookieJar|https://metacpan.org/pod/Mojo::UserAgent::CookieJar>

This module owes a debt of gratitude to their authors and those who have
contributed to them, and to their choice to make their code and work
publicly available.

=head2 COPYRIGHT AND LICENSE

Copyright 2022 José Joaquín Atria

This library is free software; you can redistribute it and/or modify it
under the Artistic License 2.0.

=end pod
