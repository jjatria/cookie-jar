#!/usr/bin/env raku

use Test;
use Cookie::Jar;

subtest 'Empty cookie jar' => { test Cookie::Jar.new }

subtest 'Load as constructor' => {
    my $out = "t/save-{ (^10).pick(10).join }.out".IO;
    LEAVE try $out.unlink;

    with Cookie::Jar.new {
        .add: 'http://www.example.com/', 'foo=bar; Max-Age=1024';
        .save: $out;
    }

    is Cookie::Jar.load($out).dump{ * ; * ; * }.elems, 1, 'Read cookies into storage';
}

subtest 'Read files without headers' => {
    my $out = "t/save-{ (^10).pick(10).join }.out".IO;
    LEAVE try $out.unlink;

    $out.spurt: q:to/SET/;
        foo=123; Path=/; Domain=foo.com;;; me=ta
        # Ignore

        # ^ Ignore blanks too
        bar=456; Path=/; Domain=foo.com;;; me=ta
        SET

    is Cookie::Jar.load($out).dump{ * ; * ; * }.elems, 2, 'Read cookies into storage';
}

subtest 'Die on bad input' => {
    my $out = "t/save-{ (^10).pick(10).join }.out".IO;
    LEAVE try $out.unlink;

    $out.spurt: 'What in tarnation';

    throws-like { Cookie::Jar.load: $out },
        X::AdHoc, message => / 'What in tarnation' $/;
}

subtest 'Netscape file format' => {
    my $out = "t/save-{ (^10).pick(10).join }.out".IO;
    LEAVE try $out.unlink;

    with Cookie::Jar.new {
        .add: 'http://www.example.com/', 'foo=bar; Max-Age=1024';
        .save: $out, :netscape;
    }

    .&is: '# Netscape HTTP Cookie File', 'Write file header'
        with $out.open.get;

    is Cookie::Jar.load($out).dump{ * ; * ; * }.elems, 1,
        'Handle header implicitly';

    is Cookie::Jar.load($out, :netscape).dump{ * ; * ; * }.elems, 1,
        'Handle header explicitly';
}

subtest 'Roundtrip' => {
    my $jar = Cookie::Jar.new;
    $jar.add( "http://www.example.com/", $_ ) for (
        'persistent=true; Max-Age=3600',
        'not=persistent',
    );

    my $total      = $jar.dump{ * ; * ; * }.elems;
    my $persistent = $jar.dump{ * ; * ; * }.grep( *.persistent ).elems;

    is test($jar).dump{ * ; * ; * }.elems, $persistent,
        'Got only persistent cookies';

    is test($jar, :all).dump{ * ; * ; * }.elems, $total,
        'Got all cookies';
}

done-testing;

sub test ( Cookie::Jar $src, $tgt = Cookie::Jar.new, :$all --> Cookie::Jar ) {
    my $out = "t/save-{ (^10).pick(10).join }.out".IO;
    LEAVE try $out.unlink;

    my $have = $src.save($out, :$all).dump;
    my $want = $tgt.load($out).dump;

    # Keep only persistent cookies
    unless $all {
        for $have{ * ; * } -> $store {
            $store{ $store.pairs.grep( *.value.persistent.not ).map: *.key }:delete;
        }
    }

    # Delete empty containers
    for $have.pairs -> $domain {
        for $domain.value.pairs -> $path {
            $have{ $domain.key }{ $path.key }:delete unless $path.value.elems;
        }

        $have{ $domain.key }:delete unless $domain.value.elems;
    }

    # Delete the keys we don't care about
    .{ *; *; * }.map: {
        $_ .= Hash;
        .< creation-time last-access-time expires >:delete
    } for $have, $want;

    is-deeply $have, $want, 'Cookie storage matches'
        or ( try require JSON::Fast ) !=== Nil and do {
            require JSON::Fast;
            diag "WANT: { JSON::Fast::to-json $want }";
            diag "HAVE: { JSON::Fast::to-json $have }";
        }

    $tgt;
}
