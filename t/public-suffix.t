#!/usr/bin/env raku

use Test;
use Cookie::Jar;

if ( try require PublicSuffix ) === Nil {
    say '1..0 # SKIP: Test requires PublicSuffix module';
    exit;
}

for (
    {
        label   => 'host is public suffix',
        url     => 'http://com.au/',
        cookies => ['SID=31d4d96e407aad42; Domain=com.au'],
        store   => {
            'com.au' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'com.au',
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'host is suffix of public suffix',
        url => 'http://au/',
        cookies => ['SID=31d4d96e407aad42; Domain=au'],
        store   => {
            'au' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'au',
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'host is unrecognized single level',
        url => 'http://localhost/',
        cookies => ['SID=31d4d96e407aad42; Domain=localhost'],
        store   => {
            'localhost' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'localhost',
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'cookie is public suffix',
        url => 'http://example.com.au/',
        cookies => ['SID=31d4d96e407aad42; Domain=com.au'],
        store   => {},
    },
    {
        label   => 'cookie is suffix of public suffix',
        url => 'http://example.com.au/',
        cookies => ['SID=31d4d96e407aad42; Domain=au'],
        store   => {},
    },
) -> ( :$label, :$url, :store($want), :$cookies )  {
    my $have = do given Cookie::Jar.new -> $jar {
        $jar.add: $url, $_ for |$cookies;
        $jar.dump;
    }

    # Delete the keys we don't care about for every cookie,
    # in every path, in every domain
    $have{ * ; * ; * }.map: {
        $_ .= Hash;
        .< creation-time last-access-time expires >:delete;
    }

    is-deeply $have, $want, $label or ( try require JSON::Fast ) !=== Nil and do {
        require JSON::Fast;
        diag "WANT: { JSON::Fast::to-json $want }";
        diag "HAVE: { JSON::Fast::to-json $have }";
    }
}

done-testing;
