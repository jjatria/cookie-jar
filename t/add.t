#!/usr/bin/env raku

use Test;
use Cookie::Jar;

for (
    {
        label   => 'no cookies',
        url     => 'http://example.com/',
        cookies => [],
        store   => {},
    },
    {
        label   => 'simple key=value',
        url     => 'http://example.com/',
        cookies => ['SID=31d4d96e407aad42'],
        store   => {
            'example.com' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'example.com',
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'secure cookie',
        url     => 'https://example.com/',
        cookies => ['SID=31d4d96e407aad42; Secure'],
        store   => {
            'example.com' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'example.com',
                        hostonly => True,
                        secure   => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'simple key=value with extensions',
        url     => 'http://example.com/',
        cookies => ['SID=31d4d96e407aad42; Value=wat; Name=boop; SameSite=bloobs; Creation-Time; Last-Access-Time=yesterday'],
        store   => {
            'example.com' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'example.com',
                        hostonly => True,
                        path     => '/',
                        extensions => {
                            Creation-Time    => True,
                            Last-Access-Time => 'yesterday',
                            Name             => 'boop',
                            SameSite         => 'bloobs', # not validated
                            Value            => 'wat',
                        },
                    }
                }
            },
        },
    },
    {
        label   => 'cookie with extraneous spaces',
        url     => 'http://example.com/',
        cookies => [' SID = dead beef '],
        store   => {
            'example.com' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => 'dead beef',
                        domain   => 'example.com',
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'international domains are canonicalised',
        url     => 'http://eñe.cl/',
        cookies => ['SID=31d4d96e407aad42'],
        store   => {
            'xn--ee-zja.cl' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'xn--ee-zja.cl',
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'invalid cookie not stored',
        url     => 'http://example.com/',
        cookies => [
            ';',
            'foo',
        ],
        store   => {},
    },
    {
        label   => 'localhost treated as host only',
        url     => 'http://localhost/',
        cookies => ['SID=31d4d96e407aad42; dOmAiN=localhost'],
        store   => {
            'localhost' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'localhost', # names are case insensitive
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'single domain level treated as host only',
        url     => 'http://foobar/',
        cookies => ['SID=31d4d96e407aad42; Domain=foobar'],
        store   => {
            'foobar' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'foobar',
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'different domain not stored',
        url     => 'http://example.com/',
        cookies => ['SID=31d4d96e407aad42; Domain=example.org'],
        store   => {},
    },
    {
        label   => 'subdomain not stored',
        url     => 'http://example.com/',
        cookies => ['SID=31d4d96e407aad42; Domain=www.example.com'],
        store   => {},
    },
    {
        label   => 'superdomain stored',
        url     => 'http://www.example.com/',
        cookies => ['SID=31d4d96e407aad42; Domain=example.com'],
        store   => {
            'example.com' => {
                '/' => {
                    SID => {
                        name   => 'SID',
                        value  => '31d4d96e407aad42',
                        domain => 'example.com',
                        path   => '/',
                    },
                },
            },
        },
    },
    {
        label   => 'path prefix /foo/ stored',
        url     => 'http://www.example.com/foo/bar',
        cookies => ['SID=31d4d96e407aad42; Path=/foo/'],
        store   => {
            'www.example.com' => {
                '/foo/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'www.example.com',
                        hostonly => True,
                        path     => '/foo/',
                    }
                }
            },
        },
    },
    {
        label   => 'path prefix /foo stored',
        url     => 'http://www.example.com/foo/bar',
        cookies => ['SID=31d4d96e407aad42; Path=/foo'],
        store   => {
            'www.example.com' => {
                '/foo' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'www.example.com',
                        hostonly => True,
                        path     => '/foo',
                    }
                }
            },
        },
    },
    {
        label   => 'last cookie wins',
        url     => 'http://example.com/',
        cookies => [ 'SID=31d4d96e407aad42', 'SID=0000000000000000', ],
        store   => {
            'example.com' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '0000000000000000',
                        domain   => 'example.com',
                        hostonly => True,
                        path     => '/',
                    }
                }
            },
        },
    },
    {
        label   => 'expired supercedes prior',
        url     => 'http://example.com/',
        cookies => [ 'SID=31d4d96e407aad42', 'SID=0000000000000000; Max-Age=-60', ],
        store   => { },
    },
    {
        label   => 'max-age ignored if not a number',
        url     => 'http://example.com/',
        cookies => [ 'foo=bar; Max-Age=not a number; HttpOnly', ],
        store   => {
            'example.com' => {
                '/' => {
                    foo => {
                        name     => 'foo',
                        value    => 'bar',
                        domain   => 'example.com',
                        hostonly => True,
                        httponly => True, # Did not skip entire cookie
                        path     => '/',
                    }
                },
            },
        },
    },
    {
        label   => 'separated by path',
        url     => 'http://example.com/foo/bar',
        cookies => [ 'SID=31d4d96e407aad42; Path=/', 'SID=0000000000000000', ],
        store   => {
            'example.com' => {
                '/' => {
                    SID => {
                        name     => 'SID',
                        value    => '31d4d96e407aad42',
                        domain   => 'example.com',
                        hostonly => True,
                        path     => '/',
                    }
                },
                '/foo' => {
                    SID => {
                        name     => 'SID',
                        value    => '0000000000000000',
                        domain   => 'example.com',
                        hostonly => True,
                        path     => '/foo',
                    }
                }
            },
        },
    },
    # check that Max-Age supercedes Expires and that Max-Age <= 0 forces
    # expiration
    {
        label   => 'max-age supercedes expires',
        url     => 'http://example.com/',
        cookies => [
            'lang=en-us; Max-Age=100; Expires=Thu, 1 Jan 1970 00:00:00 GMT',
            'SID=0000000000000000; Expires=Thu, 3 Jan 4841 00:00:00 GMT',
            'SID=31d4d96e407aad42; Max-Age=0; Expires=Thu, 3 Jan 4841 00:00:00 GMT',
            'FOO=0000000000000000; Max-Age=-100; Expires=Thu, 3 Jan 4841 00:00:00 GMT',
        ],
        store => {
            'example.com' => {
                '/' => {
                    lang => {
                        name     => 'lang',
                        value    => 'en-us',
                        domain   => 'example.com',
                        hostonly => True,
                        path     => '/',
                    },
                },
            },
        },
    },
) -> ( :$label, :$url, :store($want), :$cookies )  {
    my $have = do given Cookie::Jar.new -> $jar {
        $jar.add: $url, $_ for |$cookies;
        $jar.dump;
    }

    # Delete the keys we don't care about for every cookie,
    # in every path, in every domain
    $have{ * ; * ; * }.map: {
        $_ .= Hash;
        .< creation-time last-access-time expires >:delete;
    }

    is-deeply $have, $want, $label or ( try require JSON::Fast ) !=== Nil and do {
        require JSON::Fast;
        diag "WANT: { JSON::Fast::to-json $want }";
        diag "HAVE: { JSON::Fast::to-json $have }";
    }
}

done-testing;
