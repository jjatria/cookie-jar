#!/usr/bin/env raku

use Test;
use Cookie::Jar;

for (
    {
        label   => 'delete all cookies',
        capture => \(),
        cookies => {},
    },
    {
        label   => 'delete cookies for missing domain',
        capture => \( 'https://missing.com' ),
        cookies => {
            'example.com' => {
                '/'      => { :1plain, :2domain       },
                '/child' => { :3child, :4child-domain },
            },
            'www.example.com' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
            'example.org' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
        },
    },
    {
        label   => 'delete cookies for extant domain',
        capture => \( 'https://example.com' ),
        cookies => {
            'example.com' => {
                '/child' => { :3child, :4child-domain },
            },
            'www.example.com' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
            'example.org' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
        },
    },
    {
        label   => 'delete cookies for extant domain by name',
        capture => \( 'https://example.com', 'domain' ),
        cookies => {
            'example.com' => {
                '/'      => { :1plain                 },
                '/child' => { :3child, :4child-domain },
            },
            'www.example.com' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
            'example.org' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
        },
    },
    {
        label   => 'delete cookies for child of extant domain by name list',
        capture => \( 'https://example.com/child', < plain child-domain > ),
        cookies => {
            'example.com' => {
                '/'      => { :2domain },
                '/child' => { :3child  },
            },
            'www.example.com' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
            'example.org' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
        },
    },
    {
        label   => 'delete cookies only for child of extant domain',
        capture => \( 'https://example.com/child' ),
        cookies => {
            'www.example.com' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
            'example.org' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
        },
    },
    {
        label   => 'delete cookies for extant subdomain',
        capture => \( 'https://www.example.com' ),
        cookies => {
            'example.com' => {
                '/'      => { :1plain                 },
                '/child' => { :3child, :4child-domain },
            },
            'www.example.com' => {
                '/child' => { :3child },
            },
            'example.org' => {
                '/'      => { :1plain },
                '/child' => { :3child },
            },
        },
    },
) -> ( :$label, :$capture, :cookies($want) )  {
    my $jar = Cookie::Jar.new;

    for < example.com example.org www.example.com > -> $domain {
        $jar.add: "https://$domain", 'plain=1';
        $jar.add: "https://$domain", 'domain=2; Domain=example.com';
        $jar.add: "https://$domain", 'child=3; Path=/child';
        $jar.add: "https://$domain", 'child-domain=4; Path=/child; Domain=example.com';
    }

    subtest $label => {
        ok $jar.dump.elems, 'Cookie store has elements';

        my $have = $jar.clear( |$capture ).dump;

        # domain  path   cookies
        #     \    |    /
        $have{ * ; * ; * }.map: { $_ = .value.Int }

        is-deeply $have, $want, $label or ( try require JSON::Fast ) !=== Nil and do {
            require JSON::Fast;
            diag "WANT: { JSON::Fast::to-json $want }";
            diag "HAVE: { JSON::Fast::to-json $have }";
        }
    }
}

done-testing;
