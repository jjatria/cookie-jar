#!/usr/bin/env raku

use Test;
use Cookie::Jar;

for (
    {
        label   => 'get cookies for domain',
        capture => \( 'GET', 'https://example.com' ),
        cookies => [
            {
                name   => 'plain',
                value  => '1',
                path   => '/',
                domain => 'example.com',
                hostonly => True,
            },
            {
                name   => 'domain',
                value  => '2',
                path   => '/',
                domain => 'example.com',
            },
        ],
        header => 'plain=1; domain=2',
    },
    {
        label   => 'get cookies for subdomain',
        capture => \( 'GET', 'https://www.example.com' ),
        cookies => [
            {
                name   => 'domain',
                value  => '2',
                path   => '/',
                domain => 'example.com',
            },
            {
                name   => 'plain',
                value  => '1',
                path   => '/',
                domain => 'www.example.com',
                hostonly => True,
            },
        ],
        header => 'domain=2; plain=1',
    },
    {
        label   => 'get cookies for domain child path',
        capture => \( 'GET', 'https://example.com/child' ),
        cookies => [
            {
                name   => 'child',
                value  => '3',
                path   => '/child',
                domain => 'example.com',
                hostonly => True,
            },
            {
                name   => 'child-domain',
                value  => '4',
                path   => '/child',
                domain => 'example.com',
            },
            {
                name   => 'plain',
                value  => '1',
                path   => '/',
                domain => 'example.com',
                hostonly => True,
            },
            {
                name   => 'domain',
                value  => '2',
                path   => '/',
                domain => 'example.com',
            },
        ],
        header => 'child=3; child-domain=4; plain=1; domain=2',
    },
    {
        label   => 'get cookies for subdomain child path',
        capture => \( 'GET', 'https://www.example.com/child' ),
        cookies => [
            {
                name   => 'child-domain',
                value  => '4',
                path   => '/child',
                domain => 'example.com',
            },
            {
                name   => 'child',
                value  => '3',
                path   => '/child',
                domain => 'www.example.com',
                hostonly => True,
            },
            {
                name   => 'domain',
                value  => '2',
                path   => '/',
                domain => 'example.com',
            },
            {
                name   => 'plain',
                value  => '1',
                path   => '/',
                domain => 'www.example.com',
                hostonly => True,
            },
        ],
        header => 'child-domain=4; child=3; domain=2; plain=1',
    },
    {
        label   => 'get cookies for missing domain',
        capture => \( 'GET', 'https://missing.com' ),
        cookies => [],
        header => '',
    },
) -> ( :$label, :$capture, :cookies($want), :$header )  {
    my $jar = Cookie::Jar.new;

    for < example.com www.example.com > -> $domain {
        $jar.add: "https://$domain", 'plain=1';
        $jar.add: "https://$domain", 'domain=2; Domain=example.com';
        $jar.add: "https://$domain", 'child=3; Path=/child';
        $jar.add: "https://$domain", 'child-domain=4; Path=/child; Domain=example.com';
    }

    my $have = $jar.get(|$capture).map: {
        # Delete the keys we don't care about
        my $hash = .Hash;
        $hash< creation-time last-access-time expires >:delete;
        $hash;
    }

    subtest $label => {
        is-deeply $have, $want.List, 'Get cookies' or ( try require JSON::Fast ) !=== Nil and do {
            require JSON::Fast;
            diag "WANT: { JSON::Fast::to-json $want }";
            diag "HAVE: { JSON::Fast::to-json $have }";
        }

        is $jar.header( |$capture ), $header, 'Generated header';
    };
}

subtest 'Cookie attribute reader' => {
    my ($cookie) = do given Cookie::Jar.new {
        .add: 'https://example.com', 'foo=bar; HttpOnly; Creation-Time=now; SameSite=Lax; Flag';
        .get: 'GET', 'https://example.com';
    }

    is $cookie.get('vAluE'),    'bar', 'Can call methods case insensitively';
    is $cookie.get('SAMESITE'), 'Lax', 'Can read extensions case insesitively';
    is $cookie.get('flag'),     True,  'Flags are rendered as Bool';
    is $cookie.get('missing'),  False, 'Missing attributes are False';

    is $cookie.get('creation-time'), 'now',  'Prefer extensions';
    is $cookie.creation-time.WHAT, DateTime, 'Internal methods are still available';

    subtest 'Bad inputs' => {
        throws-like { $cookie.get }, X::AdHoc,
            message => / ^ 'Too few positionals' /;

        throws-like { $cookie.get: 123 }, X::TypeCheck::Binding::Parameter,
            message => / 'expected Str but got Int' /;

        throws-like { $cookie.get: Str }, X::Parameter::InvalidConcreteness,
            message => / 'must be an object instance' /;

        throws-like { $cookie.get: '' }, X::AdHoc,
            message => / 'attribute without a name' /;
    }

    subtest 'Aliased flags' => {
        is $cookie.get($_), True, $_ for < HttpOnly HostOnly Http-Only Host-Only >;
        is $cookie."$_"(),  True, $_ for < httponly hostonly http-only host-only >;
    }
}

subtest 'Bad inputs' => {
    my $jar = Cookie::Jar.new;
    $jar.add: 'https://example.com', 'foo=bar; Path=/; Creation-Time=now; SameSite=Lax; Flag';

    throws-like { $jar.get }, X::AdHoc,
        message => / ^ 'Too few positionals' /;

    throws-like { $jar.get: 123, 'foo' }, X::TypeCheck::Binding::Parameter,
        message => / 'expected Str but got Int' /;

    throws-like { $jar.get: 'foo', 123 }, X::TypeCheck::Binding::Parameter,
        message => / 'expected Str but got Int' /;

    throws-like { $jar.get: Str, Str }, X::Parameter::InvalidConcreteness,
        message => / 'must be an object instance' /;

    throws-like { $jar.get: 'GET', '' }, X::AdHoc,
        message => / ^ 'Cannot parse URL' /;

    throws-like { $jar.get: 'GET', 'foo' }, X::AdHoc,
        message => / ^ 'Cannot parse URL:' /;

    throws-like { $jar.get: 'GET', 'ftp://some.server' }, X::AdHoc,
        message => / ^ 'Unsupported URL scheme:' /;

    throws-like { $jar.get: 'foo', 'ftp://some.server' }, X::AdHoc,
        message => / ^ 'Unsupported HTTP method:' /;

    throws-like { $jar.get: 'get', 'ftp://some.server' }, X::AdHoc,
        message => / ^ 'Unsupported HTTP method:' /;
}

done-testing;
